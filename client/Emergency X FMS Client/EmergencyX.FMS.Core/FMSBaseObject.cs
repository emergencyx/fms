﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace EmergencyX.FMS.Core
{
	public class FMSBaseObject : INotifyPropertyChanged
	{
		// property changed event
		//
		public event PropertyChangedEventHandler PropertyChanged;


		// Implement NotifyPropertyChanged for all Objects that get this extension
		//
		public void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

	}
}
