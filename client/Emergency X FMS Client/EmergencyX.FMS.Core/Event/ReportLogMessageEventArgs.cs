﻿using EmergencyX.FMS.Core.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmergencyX.FMS.Core.Event
{
	public class ReportLogMessageEventArgs : EventArgs
	{
		private ObservableCollection<GameObject> _updatedItems;

		public ObservableCollection<GameObject> UpdatedItems
		{
			get
			{
				return _updatedItems;
			}

			private set
			{
				_updatedItems = value;
			}
		}

		public ReportLogMessageEventArgs(ObservableCollection<GameObject> items)
		{
			UpdatedItems = items;
		}
	}
}
