﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmergencyX.FMS.Core.Data
{
	public class SessionStartResponse : FMSBaseObject
	{
		private string _status;
		private string _sessionId;

		public string SessionId
		{
			get
			{
				return _sessionId;
			}

			set
			{
				_sessionId = value;
				NotifyPropertyChanged();
			}
		}

		public string Status
		{
			get
			{
				return _status;
			}

			set
			{
				_status = value;
				NotifyPropertyChanged();
			}
		}
	}
}
