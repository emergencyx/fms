﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmergencyX.FMS.Core.Data
{
	public class GameObject : FMSBaseObject
	{
		private string _name;
		private string _status;

		[JsonProperty("name")]
		public string Name
		{
			get
			{
				return _name;
			}

			set
			{
				_name = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("status")]
		public string Status
		{
			get
			{
				return _status;
			}

			set
			{
				_status = value;
				NotifyPropertyChanged();
			}
		}
	}
}
