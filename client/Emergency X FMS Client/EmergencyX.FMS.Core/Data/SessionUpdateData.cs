﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmergencyX.FMS.Core.Data
{
	public class SessionUpdateData : FMSBaseObject
	{
		private string _sessionId;
		private List<GameObject> _objects;

		[JsonProperty("sessionId")]
		public string SessionId
		{
			get
			{
				return _sessionId;
			}

			set
			{
				_sessionId = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("objects")]
		public List<GameObject> Objects
		{
			get
			{
				return _objects;
			}

			set
			{
				_objects = value;
				NotifyPropertyChanged();
			}
		}
	}
}
