﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmergencyX.FMS.Core.Data
{
	public class SessionStartData : FMSBaseObject
	{
		private string _name;
		private string _password;
		private string _style;
		private object _objects;

		[JsonProperty("name")]
		public string Name
		{
			get
			{
				return _name;
			}

			set
			{
				_name = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("password")]
		public string Password
		{
			get
			{
				return _password;
			}

			set
			{
				_password = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("style")]
		public string Style
		{
			get
			{
				return _style;
			}

			set
			{
				_style = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("objects")]
		public object Objects
		{
			get
			{
				return _objects;
			}

			set
			{
				_objects = value;
				NotifyPropertyChanged();
			}
		}
	}
}
