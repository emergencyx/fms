﻿using EmergencyX.FMS.Core.Data;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmergencyX.FMS.Core.Logic
{
	public class SessionHandler
	{
		private static string _baseUrl;

		public static string BaseUrl
		{
			get
			{
				return _baseUrl;
			}

			set
			{
				_baseUrl = value;
			}
		}

		/// <summary>
		/// Erzeugt einen RestSharp RestClient
		/// </summary>
		/// <param name="url">Basis URL für den Client</param>
		/// <returns></returns>
		private static RestClient RestClientFactory()
		{
			if (BaseUrl == null)
			{
				throw new ArgumentNullException("BaseUrl is not set");
			}

			RestClient client = new RestClient(BaseUrl);
			client.UserAgent = "Emergency X FMS Client / " + System.Reflection.Assembly.GetEntryAssembly().GetName().Version;

			return client;
		}

		/// <summary>
		/// Erzeugt einen RestSharp RestRequest mit JSON Body
		/// </summary>
		/// <param name="target"></param>
		/// <param name="method"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		private static RestRequest RestRequestFactotry(string target, Method method, string data)
		{
			RestRequest request = new RestRequest(target, method);
			request.RequestFormat = DataFormat.Json;
			request.AddJsonBody(data);

			return request;
		}

		/// <summary>
		/// Erstellt die Sessionstartquery und sendet diese an den Server
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static SessionStartResponse SessionStart(string data)
		{
			// Client zum Senden der Abfrage erstellen
			var client = RestClientFactory();

			// Requestformat vorbereiten
			var sessionCreate = RestRequestFactotry("/create", Method.POST, data);

			// senden und Rückmeldung emfangen
			IRestResponse response = client.Execute(sessionCreate);

			return JsonConvert.DeserializeObject<SessionStartResponse>(response.Content);
		}

		/// <summary>
		/// Erstellt Sessionupdates und sendet diese an den FMS Server
		/// </summary>
		/// <param name="data"></param>
		public static void SessionUpdate(string data)
		{
			var client = RestClientFactory();

			var updateSession = RestRequestFactotry("/update", Method.POST, data);

			IRestResponse response = client.Execute(updateSession);
		}
	}
}
