﻿using EmergencyX.FMS.Core.Data;
using EmergencyX.FMS.Core.Event;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace EmergencyX.FMS.Core.Logic
{
	public class LogFileHandler : FMSBaseObject
	{
		private string _logFile;
		private int _lastPosition;

		private Regex _matcher;
		private Timer _timer;

		public string LogFile
		{
			get
			{
				return _logFile;
			}

			set
			{
				_logFile = value;
				NotifyPropertyChanged();
			}
		}

		public int LastPosition
		{
			get
			{
				return _lastPosition;
			}

			set
			{
				_lastPosition = value;
				NotifyPropertyChanged();
			}
		}

		public Regex Matcher
		{
			get
			{
				return _matcher;
			}

			set
			{
				_matcher = value;
				NotifyPropertyChanged();
			}
		}

		public Timer Timer
		{
			get
			{
				return _timer;
			}

			set
			{
				_timer = value;
				NotifyPropertyChanged();
			}
		}

		public delegate void ReportLogMessages(object sender, ReportLogMessageEventArgs e);
		public event ReportLogMessages OnNewLogMessages;

		public LogFileHandler(string file)
		{
			this.LogFile = file;
			this.LastPosition = 0;
			this.Matcher = new Regex("![A-Za-z0-9]+(.+) --[0-9]{1,2}--");
		}

		public void ReadLog(object caller)
		{
			var file = new FileStream(LogFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
			var result = new ObservableCollection<GameObject>();

			using (StreamReader reader = new StreamReader(file))
			{
				if (LastPosition != 0)
				{
					// Zeilen überspringen die schon untersucht wurden
					for (int i = 0; i <= LastPosition; i++)
					{
						reader.ReadLine();
					}
				}

				string line;

				// reader.ReadLine() gibt am End null zurück, daher solange lesen bis es nichts mehr gibt.
				while ((line = reader.ReadLine()) != null)
				{
					if (line == String.Empty || line.First() != '!')
					{
						LastPosition++;
						continue;
					}

					if (!Matcher.Match(line).Success)
					{
						LastPosition++;
						continue;
					}

					var gameObj = new GameObject();
					gameObj.Status = line.Remove(0, line.Length - 6).Replace("-", "").Trim();
					gameObj.Name = line.Replace("--" + gameObj.Status + "--","").Replace("!","").Trim();
					result.Add(gameObj);
					LastPosition++;
				}
			}

			// Event erstellen, dass neue Daten zur Verfügung stehen
			var args = new ReportLogMessageEventArgs(result);
			OnNewLogMessages(this, args);

			// Timer wieder starten damit das ReadLog beim nächsten Mal wieder aufgerufen wird
			this.Timer.Change(1500, Timeout.Infinite);
		}

		public void StartTimer()
		{
			this.Timer = new Timer(this.ReadLog, null, 1500, Timeout.Infinite);
		}
	}
}
