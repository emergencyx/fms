﻿using Emergency_X_FMS_Client.Logic;
using Emergency_X_FMS_Client.ViewHelpers;
using EmergencyX.FMS.Core;
using EmergencyX.FMS.Core.Data;
using EmergencyX.FMS.Core.Event;
using EmergencyX.FMS.Core.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace Emergency_X_FMS_Client.View
{
	public class MainWindowView : FMSBaseObject
	{
		private string _sessionName;
		private string _sessionPassword;
		private string _sessionStatus;
		private string _sessionId;
		private string _sessionLink;
		private bool _isSessionLinkEnabled;

		private LogFileHandler _handler;

		private ICommand _sessionStartCommand;
		private ICommand _openLinkCommand;
		private ICommand _copyLinkCommand;

		public string SessionName
		{
			get
			{
				return _sessionName;
			}

			set
			{
				_sessionName = value;
				App.Log.Debug("Session name is " + _sessionName);
				NotifyPropertyChanged();
			}
		}

		public string SessionPassword
		{
			get
			{
				return _sessionPassword;
			}

			set
			{
				_sessionPassword = value;
				App.Log.Debug("Session password is " + _sessionPassword);
				NotifyPropertyChanged();
			}
		}

		public string SessionStatus
		{
			get
			{
				return _sessionStatus;
			}

			set
			{
				_sessionStatus = value;
				App.Log.Debug("Session status is " + _sessionStatus);
				NotifyPropertyChanged();
			}
		}

		public string SessionId
		{
			get
			{
				return _sessionId;
			}

			set
			{
				_sessionId = value;
				if (this.SessionStatus == "OK")
				{
					var url = Properties.Settings.Default.ServerAdress;
					if (url.EndsWith("/"))
					{
						this.SessionLink = String.Format("{0}show/{1}", url, this.SessionId);
					}
					else
					{
						this.SessionLink = String.Format("{0}/show/{1}", url, this.SessionId);
					}
				}
				App.Log.Debug("SessionId is " + _sessionId);
				NotifyPropertyChanged();
			}
		}

		public ICommand SessionStartCommand
		{
			get
			{
				if (_sessionStartCommand == null)
				{
					_sessionStartCommand = new RelayCommand(o => SessionStart());
				}
				return _sessionStartCommand;
			}

			set
			{
				_sessionStartCommand = value;
			}
		}

		public ICommand OpenLinkCommand
		{
			get
			{
				if(_openLinkCommand == null)
				{
					_openLinkCommand = new RelayCommand(o => {
						this.OpenLink(); 
					});
				}
				return _openLinkCommand;
			}

			set
			{
				_openLinkCommand = value;
				NotifyPropertyChanged();
			}
		}

		public ICommand CopyLinkCommand
		{
			get
			{
				if(_copyLinkCommand == null)
				{
					_openLinkCommand = new RelayCommand(o => { this.CopyLink(); });
				}
				return _openLinkCommand;
			}

			set
			{
				_openLinkCommand = value;
				NotifyPropertyChanged();
			}
		}

		public LogFileHandler Handler
		{
			get
			{
				return _handler;
			}

			set
			{
				_handler = value;
				NotifyPropertyChanged();
			}
		}

		public string SessionLink
		{
			get
			{
				return _sessionLink;
			}

			set
			{
				_sessionLink = value;
				if(_sessionLink != null)
				{
					this.IsSessionLinkEnabled = true;
				}
				else
				{
					this.IsSessionLinkEnabled = false;
				}

				App.Log.Debug("Session link is " + _sessionLink);
				NotifyPropertyChanged();
			}
		}

		public bool IsSessionLinkEnabled
		{
			get
			{
				return _isSessionLinkEnabled;
			}

			set
			{
				_isSessionLinkEnabled = value;
				App.Log.Debug("IsSessionLinkEnabled is " + _isSessionLinkEnabled);
				NotifyPropertyChanged();
			}
		}

		public MainWindowView()
		{
			App.Log.Info("App started");

			// default app state
			this.SessionPassword = "secret";
			this.IsSessionLinkEnabled = false;

			// The FMS requieres TLS 1.2 which is not always enabled => activate it but also keep TLS 1.1 active
			ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

			// Check for Updates
			var versionChecker = new UpdateCheck(@"https://gitlab.com/api/v4/projects/2249595/releases");

			if (versionChecker.IsCurrentVersionOutDated())
			{
				if(MessageBox.Show(Properties.Resources.MessageUpdateAvailable, "Emergency X FMS Client Update", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
				{
					Process.Start(@"https://gitlab.com/emergencyx/fms/-/releases");
				}
			}
		}

		/// <summary>
		/// Wird aufgerufen, wenn auf den Session starten Button geklickt wird (von dem Session Start Command)
		/// </summary>
		public void SessionStart()
		{
			App.Log.Info("Try session start");

#if !DEBUG
			if (Properties.Settings.Default.ServerAdress == String.Empty || Properties.Settings.Default.EM4LogFile == String.Empty || Properties.Settings.Default.GameObjectConfiguration == String.Empty)
			{
				var message = String.Format("Error: Values in Emergency X FMS Client.exe.config are missing. ServerAdress is: {0}, EM4LogFile is: {1}, Config File is: {2}", Properties.Settings.Default.ServerAdress, Properties.Settings.Default.EM4LogFile, Properties.Settings.Default.GameObjectConfiguration);
				System.Windows.MessageBox.Show(message, "Emergency X FMS Client", System.Windows.MessageBoxButton.OK);
				App.Log.Error(message);
				return;
			}

			if(!System.IO.File.Exists(Properties.Settings.Default.EM4LogFile))
			{
				var message = String.Format("Error: File {0} not found. Please make sure the path for {1} was correctly configured in the Emergency X FMS Client.exe.config.", Properties.Settings.Default.EM4LogFile, "EM4LogFile");
				System.Windows.MessageBox.Show(message, "Emergency X FMS Client", System.Windows.MessageBoxButton.OK);
				App.Log.Error(message);
				return;
			}

			if (Properties.Settings.Default.ServerAdress == String.Empty)
			{
				var message = String.Format("Error: File {0} not found. Please make sure the path for {1} was correctly configured in the Emergency X FMS Client.exe.config.", Properties.Settings.Default.ServerAdress, "ServerAdress");
				System.Windows.MessageBox.Show(message, "Emergency X FMS Client", System.Windows.MessageBoxButton.OK);
				App.Log.Error(message);
				return;
			}
#endif
			try
			{
				var data = new SessionStartData();
				data.Name = this.SessionName;
				data.Password = this.SessionPassword;
				data.Style = Properties.Settings.Default.StyleSheet;
				data.Objects = JsonConvert.DeserializeObject<object>(System.IO.File.ReadAllText(Properties.Settings.Default.GameObjectConfiguration));

				App.Log.Info(data);

				SessionHandler.BaseUrl = Properties.Settings.Default.ServerAdress;
				var response = SessionHandler.SessionStart(JsonConvert.SerializeObject(data));

				App.Log.Info(response);

				this.SessionStatus = response.Status;
				if (this.SessionStatus == "OK")
				{
					this.SessionId = response.SessionId;
					NotifyPropertyChanged("GetFullSessionLink");
				}
			}
			catch (Exception e)
			{

				App.Log.Error("Error in SessionStart()", e);
			}

			try
			{
				// Session Update Interval
#if !DEBUG
				this.Handler = new LogFileHandler(Properties.Settings.Default.EM4LogFile);
#else
				this.Handler = new LogFileHandler(@"D:\Program Files (x86)\Steam\steamapps\common\EMERGENCY 4 Deluxe\logfile.txt");
#endif
				this.Handler.StartTimer();
				this.Handler.OnNewLogMessages += new LogFileHandler.ReportLogMessages(RunSessionUpdater);
			}
			catch (Exception e)
			{
				App.Log.Error("Error on Handler", e);
			}
		}

		/// <summary>
		/// Event Handler für das Event, welches ausgelöst wird, wenn neue relevante Log-Einträge gefunden wurden
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void RunSessionUpdater(object sender, ReportLogMessageEventArgs e)
		{
			App.Log.Info("Run Session Updater");

			// Relevante Daten für das Update der Session beim Server
			var sessionUpdateData = new SessionUpdateData();
			sessionUpdateData.SessionId = this.SessionId;
			var objects = e.UpdatedItems.ToList<GameObject>();

			// Kein Update nötig?
			if(objects.Count == 0)
			{
				App.Log.Info("Zero items to update");
				return;
			}

			// Der Server aktzeptiert nicht mehr als 100 Updates zur selben Zeit => Beschränkung auf 999 Stk.
			var requestData = new List<GameObject>();
			int counter = 0;

			App.Log.Info(String.Format("Updater item count is {0}", objects.Count.ToString()));

			for (int i = 0; i < objects.Count; i++)
			{
				requestData.Add(objects[i]);
				if((i % 999 == 0 && i != 0) || i == objects.Count - 1)
				{
					try
					{
						sessionUpdateData.Objects = requestData;
						App.Log.Info(sessionUpdateData);
						SessionHandler.SessionUpdate(JsonConvert.SerializeObject(sessionUpdateData));
						requestData = new List<GameObject>();
					}
					catch (Exception er)
					{

						App.Log.Error(er);
					}
					counter++;
				}
			}
		}

		/// <summary>
		/// Öffnet den Session Link im Browser
		/// </summary>
		private void OpenLink()
		{
			if(this.SessionStatus == "OK" && this.SessionId != String.Empty)
			{
				try
				{
					App.Log.Info(String.Format("Link opening: {0}", this.SessionLink));
					Process.Start(this.SessionLink);
				}
				catch (Exception e)
				{

					App.Log.Info(e);
				}
				
			}
		}
		
		/// <summary>
		/// Copies the session link to the clipboard
		/// </summary>
		private void CopyLink()
		{
			try
			{
				System.Windows.Clipboard.SetText(this.SessionLink);
			}
			catch (Exception e)
			{

				App.Log.Error(e);
			}
		}
	}
}
