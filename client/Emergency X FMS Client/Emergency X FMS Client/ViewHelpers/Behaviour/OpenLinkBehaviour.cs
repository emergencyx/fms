﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Emergency_X_FMS_Client.ViewHelpers.Behaviour
{
	/// <summary>
	/// https://stackoverflow.com/q/47386442
	/// </summary>
	public class OpenLinkBehaviour
	{
		public static readonly DependencyProperty OpenLinkCommandProperty = DependencyProperty.RegisterAttached("OpenLinkCommand", typeof(ICommand), typeof(OpenLinkBehaviour), new FrameworkPropertyMetadata(new PropertyChangedCallback(OpenLinkCommandChanged)));

		private static void OpenLinkCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			FrameworkElement element = (FrameworkElement)d;
			element.MouseDown += new MouseButtonEventHandler(Element_MouseDown);
		}

		private static void Element_MouseDown(object sender, MouseButtonEventArgs e)
		{
			FrameworkElement element = (FrameworkElement)sender;
			ICommand command = GetOpenLinkCommand(element);
			command.Execute(e);
		}

		public static void SetOpenLinkCommand(UIElement element, ICommand command)
		{
			element.SetValue(OpenLinkCommandProperty, command);
		}

		private static ICommand GetOpenLinkCommand(FrameworkElement element)
		{
			return (ICommand)element.GetValue(OpenLinkCommandProperty);
		}
	}
}
