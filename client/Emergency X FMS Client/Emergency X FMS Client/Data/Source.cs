﻿using EmergencyX.FMS.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emergency_X_FMS_Client.Data
{
	public class Source : FMSBaseObject
	{
		private string _format;
		private string _url;

		[JsonProperty("format")]
		public string Format
		{
			get
			{
				return _format;
			}

			set
			{
				_format = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("url")]
		public string Url
		{
			get
			{
				return _url;
			}

			set
			{
				_url = value;
				NotifyPropertyChanged();
			}
		}
	}
}
