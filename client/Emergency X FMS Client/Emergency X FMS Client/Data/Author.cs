﻿using EmergencyX.FMS.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emergency_X_FMS_Client.Data
{
	public class Author : FMSBaseObject
	{
		private int _id;
		private string _name;
		private string _username;
		private string _state;
		private string _avatarUrl;
		private string _webUrl;

		[JsonProperty("id")]
		public int Id
		{
			get
			{
				return _id;
			}

			set
			{
				_id = value;
				NotifyPropertyChanged();
				
			}
		}

		[JsonProperty("name")]
		public string Name
		{
			get
			{
				return _name;
			}

			set
			{
				_name = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("username")]
		public string Username
		{
			get
			{
				return _username;
			}

			set
			{
				_username = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("state")]
		public string State
		{
			get
			{
				return _state;
			}

			set
			{
				_state = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("avatar_url")]
		public string AvatarUrl
		{
			get
			{
				return _avatarUrl;
			}

			set
			{
				_avatarUrl = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("web_url")]
		public string WebUrl
		{
			get
			{
				return _webUrl;
			}

			set
			{
				_webUrl = value;
				NotifyPropertyChanged();
			}
		}
	}
}
