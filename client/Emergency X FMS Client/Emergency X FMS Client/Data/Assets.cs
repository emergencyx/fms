﻿using EmergencyX.FMS.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emergency_X_FMS_Client.Data
{
	public class Assets : FMSBaseObject
	{
		private int _count;
		private List<Source> _sources;
		private List<object> _links;
		private string _evidenceFilePath;

		[JsonProperty("count")]
		public int Count
		{
			get
			{
				return _count;
			}

			set
			{
				_count = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("sources")]
		public List<Source> Sources
		{
			get
			{
				return _sources;
			}

			set
			{
				_sources = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("links")]
		public List<object> Links
		{
			get
			{
				return _links;
			}

			set
			{
				_links = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("evidence_file_path")]
		public string EvidenceFilePath
		{
			get
			{
				return _evidenceFilePath;
			}

			set
			{
				_evidenceFilePath = value;
				NotifyPropertyChanged();
			}
		}
	}
}
