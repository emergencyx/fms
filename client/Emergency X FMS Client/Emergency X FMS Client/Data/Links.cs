﻿using EmergencyX.FMS.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emergency_X_FMS_Client.Data
{
	public class Links : FMSBaseObject
	{
		private string _editUrl;

		[JsonProperty("edit_url")]
		public string EditUrl
		{
			get
			{
				return _editUrl;
			}

			set
			{
				_editUrl = value;
				NotifyPropertyChanged();
			}
		}
	}
}
