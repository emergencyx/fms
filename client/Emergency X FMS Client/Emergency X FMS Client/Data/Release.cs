﻿using EmergencyX.FMS.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emergency_X_FMS_Client.Data
{
	public class Release : FMSBaseObject
	{
		private string _name;
		private string _tagName;
		private string _description;
		private string _descriptionHtml;
		private DateTime _createdAt;
		private DateTime _realeasedAt;
		private Author _author;
		private Commit _commit;
		private bool _upcomingRelease;
		private string _commitPath;
		private string _tagPath;
		private string _evidenceSha;
		private Assets _assets;
		private Links _links;

		[JsonProperty("name")]
		public string Name
		{
			get
			{
				return _name;
			}

			set
			{
				_name = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("tag_name")]
		public string TagName
		{
			get
			{
				return _tagName;
			}

			set
			{
				_tagName = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("description")]
		public string Description
		{
			get
			{
				return _description;
			}

			set
			{
				_description = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("description_html")]
		public string DescriptionHtml
		{
			get
			{
				return _descriptionHtml;
			}

			set
			{
				_descriptionHtml = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("created_at")]
		public DateTime CreatedAt
		{
			get
			{
				return _createdAt;
			}

			set
			{
				_createdAt = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("released_at")]
		public DateTime RealeasedAt
		{
			get
			{
				return _realeasedAt;
			}

			set
			{
				_realeasedAt = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("author")]
		public Author Author
		{
			get
			{
				return _author;
			}

			set
			{
				_author = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("commit")]
		public Commit Commit
		{
			get
			{
				return _commit;
			}

			set
			{
				_commit = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("upcoming_release")]
		public bool UpcomingRelease
		{
			get
			{
				return _upcomingRelease;
			}

			set
			{
				_upcomingRelease = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("commit_path")]
		public string CommitPath
		{
			get
			{
				return _commitPath;
			}

			set
			{
				_commitPath = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("tag_path")]
		public string TagPath
		{
			get
			{
				return _tagPath;
			}

			set
			{
				_tagPath = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("evidence_sha")]
		public string EvidenceSha
		{
			get
			{
				return _evidenceSha;
			}

			set
			{
				_evidenceSha = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("assets")]
		public Assets Assets
		{
			get
			{
				return _assets;
			}

			set
			{
				_assets = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("_links")]
		public Links Links
		{
			get
			{
				return _links;
			}

			set
			{
				_links = value;
				NotifyPropertyChanged();
			}
		}
	}
}
