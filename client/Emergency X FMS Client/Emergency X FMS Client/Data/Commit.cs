﻿using EmergencyX.FMS.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emergency_X_FMS_Client.Data
{
	public class Commit : FMSBaseObject
	{
		private string _id;
		private string _shortId;
		private DateTime _createdAt;
		private List<string> _parentIds;
		private string _title;
		private string _meassage;
		private string _authorName;
		private string _authorEmail;
		private DateTime _authoredDate;
		private string _committerName;
		private string _committerEmail;
		private DateTime _committedDate;

		[JsonProperty("id")]
		public string Id
		{
			get
			{
				return _id;
			}

			set
			{
				_id = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("short_id")]
		public string ShortId
		{
			get
			{
				return _shortId;
			}

			set
			{
				_shortId = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("created_at")]
		public DateTime CreatedAt
		{
			get
			{
				return _createdAt;
			}

			set
			{
				_createdAt = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("parent_ids")]
		public List<string> ParentIds
		{
			get
			{
				return _parentIds;
			}

			set
			{
				_parentIds = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("title")]
		public string Title
		{
			get
			{
				return _title;
			}

			set
			{
				_title = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("message")]
		public string Meassage
		{
			get
			{
				return _meassage;
			}

			set
			{
				_meassage = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("author_name")]
		public string AuthorName
		{
			get
			{
				return _authorName;
			}

			set
			{
				_authorName = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("author_email")]
		public string AuthorEmail
		{
			get
			{
				return _authorEmail;
			}

			set
			{
				_authorEmail = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("authored_date")]
		public DateTime AuthoredDate
		{
			get
			{
				return _authoredDate;
			}

			set
			{
				_authoredDate = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("committer_name")]
		public string CommitterName
		{
			get
			{
				return _committerName;
			}

			set
			{
				_committerName = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("committer_email")]
		public string CommitterEmail
		{
			get
			{
				return _committerEmail;
			}

			set
			{
				_committerEmail = value;
				NotifyPropertyChanged();
			}
		}

		[JsonProperty("committed_date")]
		public DateTime CommittedDate
		{
			get
			{
				return _committedDate;
			}

			set
			{
				_committedDate = value;
				NotifyPropertyChanged();
			}
		}
	}
}
