﻿using Emergency_X_FMS_Client.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Emergency_X_FMS_Client.Logic
{
	public class UpdateCheck
	{
		private string _url;
		private readonly string _version;
			
		public string Url
		{
			get
			{
				return _url;
			}

			set
			{
				_url = value;
			}
		}

		public string Version
		{
			get
			{
				return _version;
			}
		}

		public UpdateCheck(string url)
		{
			this.Url = url;
			this._version = "v" + System.Reflection.Assembly.GetEntryAssembly().GetName().Version.ToString();
			this._version = this._version.Substring(0, this._version.Length - 2);
		}

		private string GetCurrentReleaseFromGitLab()
		{
			var webclient = new WebClient();
			var data = webclient.DownloadData(this.Url);
			List<Release> convertedData = JsonConvert.DeserializeObject<List<Release>>(Encoding.UTF8.GetString(data));
			return convertedData[0].TagName;
		}

		public bool IsCurrentVersionOutDated()
		{
			var version = this.GetCurrentReleaseFromGitLab();
			App.Log.Info($"Release Zero from GitLab is: {version}");
			App.Log.Info($"Local version is: {this.Version}");
			if (version != this.Version)
			{
				return true;
			}
			return false;
		}
	}
}
