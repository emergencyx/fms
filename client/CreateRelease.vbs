Option Explicit
Dim IsVersioNumberSet
Dim Fso
Dim ScriptPath
Dim ReleasePath
Dim BuildPath
Dim Destination
Dim LangFolder
Dim LicenseFile
Dim oFolder
Dim oFileCollection
Dim oFile
Dim oArgs
Dim ZipFile

' Ask user if he is sure, that he updated the version number of the .exe
IsVersioNumberSet = InputBox("Wurde die Versionsnummer angepasst (Ja/Nein)?", "Versionsnummer geändert")

' If not updated end
If Not IsVersioNumberSet = "Ja" Then
	Wscript.Echo "Bitte zunaechst die Versionsnummer in AssemblyInfo.cs aendern."
	Wscript.Quit(-1) ' -1 to fail the build
End If

Set Fso = CreateObject("Scripting.FileSystemObject")

' Dyn path to the release out put
ScriptPath = Fso.GetParentFolderName(WScript.ScriptFullName)
ReleasePath = Fso.BuildPath(ScriptPath, "Emergency X FMS Client")
ReleasePath = Fso.BuildPath(ReleasePath, "Emergency X FMS Client")
ReleasePath = Fso.BuildPath(ReleasePath, "bin")
ReleasePath = Fso.BuildPath(ReleasePath, "Release")

'Build directory
BuildPath = Fso.BuildPath(ScriptPath,"Deploy")

If Not Fso.FolderExists(BuildPath) Then
	Fso.CreateFolder(BuildPath)
End If

' Get files in the VS Bin/Release directory
Set oFolder = Fso.GetFolder(ReleasePath)
Set oFileCollection = oFolder.Files

For Each oFile in oFileCollection
	' Use "Do Loop" to get "continue" feature via "Exit Do"
	Do
		' Not needed files
		If Right(oFile.Name, 4) = ".pdb" Then 
			Exit Do
		End If
		
		If Right(oFile.Name, 4) = ".log" Then
			Exit Do
		End If
		
		If Right(oFile.Name,10) = "vshost.exe" Then
			Exit Do
		End If
		
		If Right(oFile.Name,17) = "vshost.exe.config" Then
			Exit Do
		End If

		If Right(oFile.Name,4) = ".xml" Then
			Exit Do
		End If

		If Right(oFile.Name,9) = ".manifest" Then
			Exit Do
		End If

		Destination = Fso.BuildPath(BuildPath, oFile.Name)
		Fso.CopyFile oFile.Path, Destination , true
		
	Loop While False
	
Next

' Langauge resource folder
LangFolder = Fso.BuildPath(ReleasePath, "de")
Destination = Fso.BuildPath(BuildPath, "de")
Fso.CopyFolder LangFolder, Destination, true

'License files of third party dll
LicenseFile = Fso.BuildPath(ScriptPath, "LICENSE-log4net.txt")
Destination = Fso.BuildPath(Buildpath, "LICENSE-log4net.txt")
Fso.CopyFile LicenseFile, Destination, true

LicenseFile = Fso.BuildPath(ScriptPath, "LICENSE-Newtonsoft.Json.txt")
Destination = Fso.BuildPath(Buildpath, "LICENSE-Newtonsoft.Json.txt")
Fso.CopyFile LicenseFile, Destination, true

LicenseFile = Fso.BuildPath(ScriptPath, "LICENSE-RestSharp.txt")
Destination = Fso.BuildPath(Buildpath, "LICENSE-RestSharp.txt")
Fso.CopyFile LicenseFile, Destination, true

'License files of EM X
LicenseFile = Fso.BuildPath(ScriptPath, "..\LICENSE")
Destination = Fso.BuildPath(Buildpath, "LICENSE.txt")
Fso.CopyFile LicenseFile, Destination, true

' ZIP everything
ZipFile = Fso.BuildPath(ScriptPath, "Emergency_X_FMS_Client.zip")

With CreateObject("Scripting.FileSystemObject")
	With .CreateTextFile(ZipFile, true)
		.Write Chr(80) & Chr(75) & Chr(5) & Chr(6) & String(18, chr(0))
	End With
End With

With CreateObject("Shell.Application")
	.NameSpace(ZipFile).CopyHere .Namespace(Buildpath).Items

	Do Until .NameSpace(ZipFile).Items.Count = .NameSpace(BuildPath).Items.Count
		WScript.Sleep 1000
	Loop
End With

WScript.Echo "Successfully created a package for production"