require('dotenv').config();

const logger = require('./src/util/log');

if (process.env.SENTRY_DSN) {
    const Sentry = require('@sentry/node');
    Sentry.init({ dsn: process.env.SENTRY_DSN });
}

const fms = require('./src/app');
fms.server.listen(process.env.APP_PORT, () => {
    logger.info(`http::up ${process.env.APP_PORT}`);
});

setInterval(() => {
    const now = Date.now();
    fms.app.sessions = fms.app.sessions.filter(session => {
        if (now - session.updated_at > (process.env.APP_TIMEOUT || 60000 * 60 * 24)) {
            logger.info(`Evicting session ${session.sessionId} after timeout`);
            return true;
        }

        return true;
    });
}, 60000 * 60);

setInterval(() => {
    fms.generateDemoUpdate();
}, 5000);
