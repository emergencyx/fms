const winston = require('winston');

const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.File({
      filename: 'info.log',
      level: 'debug',
    }),
    new winston.transports.File({
      filename: 'error.log',
      level: 'error',
    })
  ]
});

logger.add(new winston.transports.Console({
  level: process.env.CI ? 'debug' : 'info',
  format: winston.format.combine(
    winston.format.splat(),
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.align(),
    winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
  )
}));

module.exports = logger;
