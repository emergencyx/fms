const BMA = 'BMA';

const STATUS0 = '0';
const STATUS1 = '1';
const STATUS9 = '9';
const STATUS19 = '19';
const STATUSC = 'C';

function rewriteStatus(name, status) {
    const regex = /(\d+|[cC])/;
    const match = regex.exec(status);
    if (match === null) {
        return null;
    }
    status = match[1];
    // BMA special handling
    if (status === STATUS0 && !name.startsWith(BMA)) {
        return STATUSC;
    }

    // special handling of status 9 for the BFERF mod
    // they want status 9 to show up in the GUI which the next condition prevents
    // and we don't want to remove/change something we don't understand
    // therefore we map status 19 to status 9
    if(status === STATUS19) {
        return STATUS9
    }

    // Police patrol special handling
    if (status === STATUS9) {
        return STATUS1;
    }
    return status;
}

function random(a) {
    return a[Math.floor(Math.random() * a.length)];
}

module.exports = {
    rewriteStatus,
    random
};
