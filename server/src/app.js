const express = require('express');
const nanoid = require('nanoid');
const app = express();
const rewriteStatus = require('./util/status').rewriteStatus;
const randomItem = require('./util/status').random;
const logger = require('./util/log');
const WebSocket = require('ws');
const url = require('url');
const http = require('http');

const FMS_SESSION_HEADER = 'FMS-Session-Id';
const STATUS_FAIL = 'FAIL';
const FMS_SESSION_DEMO_ID = "demo";
const FMS_SESSION_DEMO_NAME = "FMS Demo Session";
const FMS_SESSION_DEMO_LOOKUP = {
    "Florian Bieberfelde 01.11.01": {
        "display": "FW 1 ELW1 1",
        "state": "2",
        "category": "Feuerwehr - Hauptwache 1",
        "name": "Florian Bieberfelde 01.11.01"
    },
    "Bieber 10.01": {
        "display": "POL 1 STW 1",
        "state": "6",
        "category": "Schutzpolizei",
        "name": "Bieber 10.01"
    },
    "BB Wald Normal": {
        "display": "Waldkrankenhaus",
        "state": "C",
        "category": "Bettenbelegung Normal",
        "name": "BB Wald Normal"
    },
    "BMA Hotel Mitte": {
        "display": "BMA Hotel Stadtmitte",
        "state": "0",
        "category": "BMA",
        "name": "BMA Hotel Mitte"
    },
};

app.sessions = [];
app.use(express.json());

const server = http.createServer(app);
const webSocket = new WebSocket.Server({ server });

function getSessionId(request) {
    const regex = /\/websocket\/(\S{10}|demo)/gm;
    const pathname = url.parse(request.url).pathname;
    const match = regex.exec(pathname);
    if (match && match.length) {
        return match[1];
    }
    return null;
}

webSocket.shouldHandle = (request) => {
    return getSessionId(request);
};

webSocket.on('connection', (ws, request) => {
    const sessionId = getSessionId(request);
    const session = app.sessions.find(session => session.sessionId === sessionId);

    ws.sessionId = sessionId;
    if (session) {
        const message = JSON.stringify({
            type: 'connect',
            timestamp: Date.now(),
            lookup: session.lookup,
            sessionName: session.sessionName
        });
        ws.send(message);
    } else if (sessionId === FMS_SESSION_DEMO_ID) {
        const message = JSON.stringify({
            type: 'connect',
            timestamp: Date.now(),
            lookup: FMS_SESSION_DEMO_LOOKUP,
            sessionName: FMS_SESSION_DEMO_NAME
        });
        ws.send(message);
    } else {
        logger.info(`Unable to find session ${sessionId}`);
    }
});

app.post('/create', function (req, res) {
    if ('objects' in req.body) {
        const sessionId = nanoid(10);
        const sessionName = req.body.name || '';
        if (sessionName.length > 100) {
            return res.status(400).json({
                status: STATUS_FAIL,
                message: 'Session name too long'
            });
        }

        const created_at = Date.now();
        const updated_at = Date.now();

        const lookup = Object.keys(req.body.objects)
            .reduce((accumulator, category) => {
                for (const object of req.body.objects[category]) {
                    accumulator[object.log] = {
                        display: object.display,
                        state: rewriteStatus(object.log, object.state),
                        category
                    };
                }
                return accumulator;
            }, {});

        app.sessions.push({ sessionId, lookup, created_at, updated_at, sessionName });
        res.setHeader(FMS_SESSION_HEADER, sessionId);
        return res.json({
            status: 'OK',
            sessionId
        });
    }

    return res.status(400).json({
        status: STATUS_FAIL,
        message: 'Missing objects in request body'
    });
});

app.post('/update', function (req, res) {
    if ('sessionId' in req.body && 'objects' in req.body) {
        const session = app.sessions.find(session => session.sessionId === req.body.sessionId);
        if (session) {
            const updated = [];
            for (const object of req.body.objects) {
                if (object.name in session.lookup) {
                    const state = rewriteStatus(object.name, object.status);
                    if (state && session.lookup[object.name].state !== state) {
                        session.lookup[object.name].state = state;
                        updated.push({ name: object.name, state });
                    } else {
                        logger.debug(`Unknown or duplicated state ${object.state}`, { session: session.sessionId, object });
                    }
                } else {
                    logger.debug(`Unknown object ${object.name}`);
                }
            }

            if (updated.length > 0) {
                const timestamp = Date.now();
                session.updated_at = timestamp;
                const message = JSON.stringify({ type: 'update', timestamp, updated });
                webSocket.clients.forEach((client) => {
                    if (client.readyState === WebSocket.OPEN && client.sessionId === session.sessionId) {
                        client.send(message);
                    }
                });
            }

            res.setHeader(FMS_SESSION_HEADER, session.sessionId);
            return res.end();
        }

        return res.status(404).end();
    }

    return res.status(400).end();
});

app.get('/api/health', (req, res) => {
    return res.end('🚀');
});

function generateDemoUpdate() {
    const timestamp = Date.now();
    const randomId = randomItem(Object.keys(FMS_SESSION_DEMO_LOOKUP));
    const randomUnit = FMS_SESSION_DEMO_LOOKUP[randomId].name;
    const randomState = randomItem(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']);
    const state = rewriteStatus(randomUnit, randomState);
    const updated = [{ name: randomUnit, state }];
    const message = JSON.stringify({ type: 'update', timestamp, updated });
    webSocket.clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN && client.sessionId === FMS_SESSION_DEMO_ID) {
            client.send(message);
        }
    });
}

module.exports = {
    app,
    server,
    generateDemoUpdate
};
