const chai = require('chai');
chai.use(require('chai-http'));
const expect = chai.expect;
const WebSocket = require('ws');
const logger = require('../src/util/log');

describe('HTTP API', function () {
  it('should create a session', function (done) {
    const objects = {
      'Feuerwehr - Hauptwache 1': [
        { 'log': 'Florian Bieberfelde 01.11.01', 'display': 'FW 1 ELW1 1', 'state': '6' },
        { 'log': 'Florian Bieberfelde 01.46.01', 'display': 'FW 1 HLF20 1', 'state': '6' },
      ],
      'BMA': [
        { 'log': 'BMA Krankenhaus', 'display': 'BMA Krankenhaus', 'state': '0' },
        { 'log': 'BMA HBF', 'display': 'BMA Hauptbahnhof', 'state': '0' },
      ]
    };

    const request = {
      name: 'Test Session',
      objects,
      scope: 'public',
      password: '',
      style: 'app.css'
    };

    const fms = require('../src/app');
    fms.app.sessions = [];

    chai.request(fms.server)
      .post('/create')
      .send(request)
      .end(function (err, res) {
        expect(err).to.equal(null);

        expect(res).to.be.json;
        expect(res).to.have.status(200);
        expect(res).to.have.header('FMS-Session-Id', fms.app.sessions[0].sessionId);
        expect(res.body.status).to.equal('OK');
        expect(res.body.sessionId).to.be.a('string').and.to.have.lengthOf(10);

        expect(fms.app.sessions).to.have.lengthOf(1);
        expect(fms.app.sessions[0].sessionName).to.equal('Test Session', 'Should show session name');
        expect(fms.app.sessions[0].sessionId).to.equal(res.body.sessionId);
        expect(fms.app.sessions[0].lookup).to.deep.equal({
          'BMA HBF': {
            category: 'BMA',
            display: 'BMA Hauptbahnhof',
            state: '0',
          },
          'BMA Krankenhaus': {
            category: 'BMA',
            display: 'BMA Krankenhaus',
            state: '0',
          },
          'Florian Bieberfelde 01.11.01': {
            category: 'Feuerwehr - Hauptwache 1',
            display: 'FW 1 ELW1 1',
            state: '6',
          },
          'Florian Bieberfelde 01.46.01': {
            category: 'Feuerwehr - Hauptwache 1',
            display: 'FW 1 HLF20 1',
            state: '6',
          }
        });

        done();
      });
  });

  it('should not create a session that has no objects', function (done) {
    const request = {
      name: 'Test Session',
      scope: 'public',
      password: '',
      style: 'app.css'
    };

    const fms = require('../src/app');
    fms.app.sessions = [];

    chai.request(fms.server)
      .post('/create')
      .send(request)
      .end(function (err, res) {
        expect(err).to.equal(null);

        expect(res).to.have.status(400);
        expect(res.body.status).to.equal('FAIL');
        expect(res.body.message).to.contain('Missing objects');
        expect(fms.app.sessions).to.be.empty;
        done();
      });
  });

  it('should not create a session that has a very long name', function (done) {
    const request = {
      name: 'aaaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeffffffffffgggggggggghhhhhhhhhhiiiiiiiiiijjjjjjjjjjkkkkkkkkkk',
      scope: 'public',
      password: '',
      style: 'app.css',
      objects: []
    };

    const fms = require('../src/app');
    fms.app.sessions = [];

    chai.request(fms.server)
      .post('/create')
      .send(request)
      .end(function (err, res) {
        expect(err).to.equal(null);

        expect(res).to.have.status(400);
        expect(res.body.status).to.equal('FAIL');
        expect(res.body.message).to.contain('too long');
        expect(fms.app.sessions).to.be.empty;
        done();
      });
  });

  it('should update a session', function (done) {
    const fms = require('../src/app');
    const sessionId = 'test5test5';
    const lookup = {
      'BMA HBF': {
        category: 'BMA',
        display: 'BMA Hauptbahnhof',
        state: '0',
      },
      'BMA Krankenhaus': {
        category: 'BMA',
        display: 'BMA Krankenhaus',
        state: '0',
      },
      'Florian Bieberfelde 01.11.01': {
        category: 'Feuerwehr - Hauptwache 1',
        display: 'FW 1 ELW1 1',
        state: '6',
      },
      'Florian Bieberfelde 01.46.01': {
        category: 'Feuerwehr - Hauptwache 1',
        display: 'FW 1 HLF20 1',
        state: '6',
      }
    };

    fms.app.sessions = [{
      sessionId,
      lookup
    }];

    const request = {
      sessionId,
      objects: [
        { name: 'Florian Bieberfelde 01.11.01', status: '2' }
      ]
    };

    chai.request(fms.server)
      .post('/update')
      .send(request)
      .end(function (err, res) {
        expect(err).to.equal(null);

        expect(res).to.have.status(200);
        expect(res).to.have.header('FMS-Session-Id', sessionId);
        expect(fms.app.sessions).to.have.lengthOf(1);
        expect(fms.app.sessions[0].lookup['Florian Bieberfelde 01.11.01']).to.deep.equal({
          category: 'Feuerwehr - Hauptwache 1',
          display: 'FW 1 ELW1 1',
          state: '2',
        });
        done();
      });
  });

  describe('Realtime', () => {
    it('should receive an initial connect message over websocket', function (done) {
      const fms = require('../src/app');
      const sessionId = 'websocket1';
      const sessionName = 'WebSocket Test Session';
      const lookup = {
        'BMA HBF': {
          category: 'BMA',
          display: 'BMA Hauptbahnhof',
          state: '0',
        }
      };
  
      fms.app.sessions = [{
        sessionId,
        sessionName,
        lookup
      }];
    
      const server = fms.server.listen(0, () => {
        const url = new URL('ws://localhost/websocket/websocket1');
        url.port = server.address().port;
        const ws = new WebSocket(url);
        ws.onopen = (e) => {
          expect(e.type).to.equal('open');
          expect(ws.readyState).to.equal(WebSocket.OPEN);
        };
        ws.onmessage = (e) => {
          expect(ws.readyState).to.equal(WebSocket.OPEN);
          expect(e.type).to.equal('message');
          expect(e.data).to.not.be.empty;
          expect(JSON.parse(e.data)).to.include({type: 'connect'});
          expect(JSON.parse(e.data)).to.include({sessionName});
          ws.close();
        };
        ws.onerror = (e) => {
          done(e.error); // registers as test fail
        };
        ws.onclose = (e) => {
          expect(e.type).to.equal('close');
          expect(e.wasClean).to.equal(true);
          server.close(done);
        }
      });
    });

    it('should have a demo session to preview different styles', function (done) {
      const fms = require('../src/app');
      const sessionName = 'FMS Demo Session';
      fms.app.sessions = [];
    
      const server = fms.server.listen(0, () => {
        const url = new URL('ws://localhost/websocket/demo');
        url.port = server.address().port;
        const ws = new WebSocket(url);
        ws.onopen = (e) => {
          expect(e.type).to.equal('open');
          expect(ws.readyState).to.equal(WebSocket.OPEN);
        };
        ws.onmessage = (e) => {
          expect(ws.readyState).to.equal(WebSocket.OPEN);
          expect(e.type).to.equal('message');
          expect(e.data).to.not.be.empty;
          expect(JSON.parse(e.data)).to.include({type: 'connect'});
          expect(JSON.parse(e.data)).to.include({sessionName});
          ws.close();
        };
        ws.onerror = (e) => {
          done(e.error); // registers as test fail
        };
        ws.onclose = (e) => {
          expect(e.type).to.equal('close');
          expect(e.wasClean).to.equal(true);
          server.close(done);
        }
      });
    });
  });
});
