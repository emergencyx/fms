const chai = require('chai');
const expect = chai.expect;
const rewriteStatus = require('../src/util/status').rewriteStatus;

describe('Status Rewriter', function () {
  it('should reject invalid states', function () {
    expect(rewriteStatus('Florian Bieberfelde 02.46.01', 'invalid')).to.be.null;
  });

  it('should pass valid states', function () {
    expect(rewriteStatus('Florian Bieberfelde 02.46.01', '2')).to.be.equal('2');
    expect(rewriteStatus('Florian Bieberfelde 02.46.01', '3')).to.be.equal('3');
    expect(rewriteStatus('Florian Bieberfelde 02.46.01', '6')).to.be.equal('6');
  });

  it('should apply BMA special handling', function () {
    expect(rewriteStatus('Florian Bieberfelde 02.46.01', '0')).to.be.equal('C');
    expect(rewriteStatus('BMA Krankenhaus', '0')).to.be.equal('0');
  });

  it('should apply police patrol special handling', function () {
    expect(rewriteStatus('Bieber 10.05', '9')).to.be.equal('1');
  });

  it('should handle "Bettenbelegung"', function () {
    expect(rewriteStatus('BB XYZ', '20')).to.be.equal('20');
  });
});

/**
  // there is no status 0 - its a C!
  if ($object['status'] == '0' && !starts_with($object['name'], 'BMA')) {
      $object['status'] = 'C';
  }

  // status 9 means patrol for police which
  // means free on radio
  if ($object['status'] == '9') {
      $object['status'] = '1';
  }
 */
